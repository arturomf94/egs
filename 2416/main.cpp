#include <stdio.h>
using namespace std; 
int sumDigits(int n, int cs)
{
    int sum = 0, m;
    while (n > 0)
    {
        m = n % 10;
        sum += m;
        if (sum > cs)
        {
            return sum;
        }
        n = n / 10;
    }
    return sum;
}
bool checkSum(int candidateI, int candidateSum)
{
    int sum = sumDigits(candidateI, candidateSum);
    if(sum == candidateSum)
    {
        return 1;
    } else
    {
        return 0;
    }
}
bool testInputs(int c, int cD)
{
    int candidateSum;
    int candidateI;
    for (candidateSum = 1; candidateSum <= 45; candidateSum ++)
    {
        candidateSum += cD;
        candidateI = c + candidateSum;
        if(checkSum(candidateI, candidateSum))
        {
            return 1;
        }
    }
    return 0;
}
int getDigit(int m)
{
    int candidateD;
    int candidateI;
    bool trueCandidate;
    for (candidateD = 1; candidateD < 10; candidateD++)
    {
        candidateI = m * 10 + candidateD;
        trueCandidate = testInputs(candidateI, candidateD);
        if (trueCandidate)
        {
            return candidateD;
        }
    }
    return -1;
}
int main()
{
    int T;
    int i;
    int j;
    scanf("%d", &T);
    int A[T];
    for (j = 0; j < T; j++)
    {
        scanf("%d", &i);
        A[j] = getDigit(i);
    }
    j = 0;
    for (j = 0; j < T; j++)
    {
        printf("%d ", A[j]);
        printf("\n");
    }
}
