use std::io;

fn solve_factors(x: u64, lower_bound: u64) -> Vec<u64>{
    let mut factors: Vec<u64> = Vec::new();
    for i in 1..x {
        if i * i > x {
            break;
        }
        if x % i == 0 {
            let factor = x / i;
            if (factor - i) % 2 != 0 || (factor + i) % 2 != 0{
                continue;
            }
            let m1 = (factor - i) / 2;
            let m2 = (factor + i) / 2;
            if m1 * m1 > lower_bound && m2 * m2 > lower_bound + x {
                factors.push(m1);
            }
        }
    }
    factors
}

fn get_partial_sequence(n: usize, x: &mut Vec<u64>, i: usize, lower_bound: u64) -> bool {
    if i == n {
        println!("Yes");
        let mut result_value: String = String::new();
        for value in x {
            result_value.push_str(&value.to_string());
            result_value.push_str(" ");
        }
        println!("{}", result_value);
        return true
    }
    let factors: Vec<u64> = solve_factors(x[i + 1], lower_bound);
    let factors_length = factors.len();
    if factors_length == 0 {
        return false
    } else {
        for factor in factors.iter().rev() {
            let found = factor * factor;
            x[i] = found - lower_bound;
            let valid = get_partial_sequence(n, x, i + 2, lower_bound + x[i] + x[i + 1]);
            if valid {
                return true
            } else {
                continue;
            }
        }
    }
    return false
}

fn get_sequence(n: usize, x: &mut Vec<u64>) {
    let valid = get_partial_sequence(n, x, 0, 0);
    if !valid {
        println!("No");
    }
}

fn main() {
    let mut n = String::new();
    let mut x_input = String::new();
    io::stdin().read_line(&mut n)
        .expect("Failed to read line");
    let n: usize = n.trim().parse()
        .expect("Not a number!");
    io::stdin().read_line(&mut x_input)
        .expect("Failed to read line");
    let split = x_input.split(" ");
    let mut x: Vec<u64> = Vec::new();
    for s in split {
        x.push(0);
        let pair_input: u64 = s
            .trim()
            .parse()
            .expect("Could not parse an input number!");
        x.push(pair_input);
    }
    get_sequence(n, &mut x);
}
