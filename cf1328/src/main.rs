use std::io;

fn get_digit(x: &mut String, i: usize) -> u32 {
    const RADIX: u32 = 10;
    x.chars()
        .nth(i)
        .unwrap()
        .to_digit(RADIX)
        .unwrap()
}

fn process_pair(n: usize, x: &mut String, v: &mut Vec<String>) {
    let mut u: String = String::new();
    let mut l: String = String::new();
    u.push_str("1");
    l.push_str("1");
    let mut d: u32;
    let mut even: bool = true;
    let mut i: usize = 1;
    loop {
        if i == n {
            break;
        }
        d = get_digit(x, i);
        if d == 0 {
            u.push_str("0");
            l.push_str("0");
        }
        if even {
            if d == 1 {
                even = false;
                u.push_str("1");
                l.push_str("0");
            }
            else if d == 2 {
                u.push_str("1");
                l.push_str("1");
            }
        } else {
            if d == 1 {
                u.push_str("0");
                l.push_str("1");
            }
            else if d == 2 {
                u.push_str("0");
                l.push_str("2");
            }
        }
        i += 1;
    }
    v.push(u);
    v.push(l);
}

fn print_vec(v: Vec<String>) {
    for i in 0..v.len() {
        println!("{}", v[i]);
    }
}

fn main() {
    let mut t = String::new();

    io::stdin().read_line(&mut t)
        .expect("Failed to read line");

    let t: u32 = t.trim().parse().expect("Not a number!");

    let mut v: Vec<String> = Vec::new();

    for _i in 0..t {
        let mut n = String::new();
        let mut x = String::new();
        io::stdin().read_line(&mut n)
            .expect("Failed to read line");
        let n: usize = n.trim().parse()
            .expect("Not a number!");
        io::stdin().read_line(&mut x)
            .expect("Failed to read line");
        process_pair(n, &mut x, &mut v);
    };

    print_vec(v);

}
