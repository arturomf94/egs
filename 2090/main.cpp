#include <iostream>
#include <cmath>
using namespace std; 

int countS(int k)
{
    int m = (int)floor (log10(k));  
    if (m > 0)
    {
        int count = 0;
        int sum = 0;
        int sumIndex;
        for (sumIndex = 1; sumIndex <= m; sumIndex++)
        {
            sum+= pow(10, sumIndex);
        }
        count += k + (k + 1) * m - sum;
        return count;
    }
    return k;
}

int * getDigit(int i, int ch)
{
    static int DR[2];
    int nextDigit = 1;
    int count = ch;
    int D;
    int N = 0;
    int d;
    while (1) 
    {
        N++;
        D = (int)floor (log10(nextDigit)) + 1;
        int digitN; 
        for (digitN = 1; digitN <= D; digitN++)
        {
            d = (int)(nextDigit / pow(10, D - digitN)) % 10; 
            count++;
            if (count == i)
            {
                DR[0] = d;
                DR[1] = N;
                return DR;
            }
        }
        nextDigit++;
    }
}

int * getResult(int i)
{
    static int R[3];
    int count = 0;
    int k = 1;
    int sk;
    while (count < i)
    { 
        sk = countS(k);
        count += sk;
        k++;
    }
    k--;
    int *DR = getDigit(i, count - sk);
    R[0] = DR[0];
    R[1] = DR[1];
    R[2] = k;
    return R;
}

int main()
{
    int T;
    int i;
    int j;
    cin >> T;
    int A[T];
    for (j = 0; j < T; j++)
    {
        cin >> i;
        A[j] = i;
    }
    j = 0;
    for (j = 0; j < T; j++)
    {
        int *R = getResult(A[j]);
        cout << R[0]  << " " << R[1] << " " << R[2] << endl;
    }
}


