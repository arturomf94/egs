#include <stdio.h>
#include <math.h>
using namespace std;

bool trialDivision(int n)
{
    if (n == 1)
    {
        return 0;
    } else
    {
        int k;
        int root = sqrt(n) ;
        for (k = 2; k <= root; k++)
        {
            if (n % k == 0)
            {
                return 0;
            }
        }
        return 1;
    }

}

int numAlmostPrimes(int low, int high)
{
    int count = 0;
    int length = high - low + 1;
    bool isPrime[length];
    int i;
    int j;
    for (i = 0; i < length; i++)
    {
        isPrime[i] = trialDivision(low + i);
    }
    for (i = 0; i < length; i++)
    {
        if (not isPrime[i])
        {
            int divPrimes = 0;
            for (j = 0; j < i; j++)
            {
                if (isPrime[j] && (low + i) % (low + j) == 0)
                {
                    divPrimes++;
                }
            }
            if (divPrimes == 1)
            {
                count ++;
            }
        }

    }
    return count;
}

int main()
{
    int T;
    int low, high;
    int j;
    scanf("%d", &T);
    int A[T];
    for (j = 0; j < T; j++)
    {
        scanf("%d %d", &low, &high);
        A[j] = numAlmostPrimes(low, high);
    }
    for (j = 0; j < T; j++)
    {
        printf("%d ", A[j]);
        printf("\n");
    }
}
